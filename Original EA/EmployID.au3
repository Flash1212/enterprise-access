;Identify Employ ID

#include <AD-original.au3>
HotKeySet("{ESC}", "_Terminate")
Do
$User = InputBox('Employee UserName',"What is the employee's UserName?")
If @error Then Exit
$attribute = 'employeeID' ;InputBox('Attribute to search',"What attribute do you want to see?")
$EmployID = _ADGetObjectAttribute($User,$attribute)

MsgBox(0,'User EmployID',$EmployID)

$ask = MsgBox(4,'Change','Do you want to modify the EmployeeID?')

If $ask = 6 Then
	$NewID = InputBox('New Employee ID','New EmployeeID for '&$User)
	If $NewID = '' Or @error = 1 Then
	Else
		_ADModifyAttribute($User,$attribute,$NewID)
		MsgBox(64,'New EmployeeID','The new employee ID for '&$User&' is :'&_ADGetObjectAttribute($User,$attribute)&'.')
	EndIf
EndIf
Until Not ProcessExists('Explorer.exe')

Func _Terminate()
	Exit
EndFunc