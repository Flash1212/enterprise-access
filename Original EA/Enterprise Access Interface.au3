;Enterprise Interface

#include <GUIConstantsEx.au3>
#include <StaticConstants.au3>
#include <GuiListView.au3>
#include <AD.au3>
#include <file.au3>
#include <array.au3>
#include <string.au3>

Global $Number_of_Users, $acleaned, $aList, $DupeSub, $load_File
Dim $Import, $oImport, $Iwrite, $Iread, $CountLines, $strImported, $aImported, $index, $nLine, $New_Employees, $ObjUser, $cnname
Dim $userou, $employID, $fname, $mname, $lname, $pin, $costcener, $department, $location
Dim $accounts, $adRead, $AD_Export, $EnterpriseLoc, $strImported, $log, $names, $Xport_List, $Timer = 0, $Accounts_Created
$EnterpriseLoc = @ScriptDir
$log = $EnterpriseLoc & '\Enterprise-Access.log'
FileDelete($log)
$AD_Export = $EnterpriseLoc & '\AD Export.txt'
$XML = $EnterpriseLoc & '\SSO_XML.xml'
$ou = 'OU=EnterpriseAccess,OU=User Accounts,DC=seton,DC=org'
$Import = $EnterpriseLoc & '\Enterprise.txt'
$write_File = $EnterpriseLoc & '\New ENTACCES.txt'
$finaloutput = $EnterpriseLoc & '\Final Output.csv'
$User_Info_Export = $EnterpriseLoc & '\AD_User_Info.csv'

Opt('GUIOnEventMode', 1)

#Region ### START Koda GUI section ### Form=
$EAI = GUICreate("Enterprise Interface", 633, 592, 193, 125)
GUISetOnEvent($GUI_EVENT_CLOSE, '_End_Script', $EAI)

; Create File menu
$File_menu = GUICtrlCreateMenu("&File")
$File_open = GUICtrlCreateMenuItem("Open File", $File_menu)
GUICtrlSetOnEvent(-1, '_Load_File')
$Close_File = GUICtrlCreateMenuItem("Close Open File", $File_menu)
GUICtrlSetOnEvent(-1, '_Close_file')
$Separator1 = GUICtrlCreateMenuItem("", $File_menu, 3)
$Exititem = GUICtrlCreateMenuItem("Exit", $File_menu)
GUICtrlSetOnEvent(-1, '_End_Script')

;Action Buttons
$Import_Into_AD = GUICtrlCreateButton("Import Into AD", 24, 32, 137, 25, 0)
GUICtrlSetOnEvent(-1, '_Import')
$OutPut_SSO_XML = GUICtrlCreateButton("Output SSO XML", 24, 115, 137, 25, 0)
GUICtrlSetOnEvent(-1, '_EnterpriseAccess_CreateSSOXML')
$CSV_of_Accounts = GUICtrlCreateButton("CSV_of_Accounts", 24, 72, 137, 25, 0)
GUICtrlSetOnEvent(-1, '_Create_CSV')
$Add_Users_To_A_Group = GUICtrlCreateButton("Add Users To A Group", 192, 32, 137, 25, 0)
GUICtrlSetOnEvent(-1, '_Add_Users_To_A_Group')
$Password_Input = GUICtrlCreateInput("password", 360, 32, 137, 25, 0)
$Export_AD_Information = GUICtrlCreateButton("Export AD Information", 192, 112, 137, 25, 0)
GUICtrlSetOnEvent(-1, '_Export_AD_Info')
$EXSvr = GUICtrlCreateCombo("None", 216, 72, 409, 25)
GUICtrlSetData(-1, '|CN=EX1SG1MBX1,CN=EX1SG1,CN=InformationStore,CN=AUSEX1VS1,CN=Servers,CN=First Administrative Group,CN=Administrative Groups,CN=SETON,CN=Microsoft Exchange,CN=Services,CN=Configuration,DC=seton,DC=org' & _
		'|CN=EX1SG1MBX2,CN=EX1SG1,CN=InformationStore,CN=AUSEX1VS1,CN=Servers,CN=First Administrative Group,CN=Administrative Groups,CN=SETON,CN=Microsoft Exchange,CN=Services,CN=Configuration,DC=seton,DC=org' & _
		'|CN=EX1SG1MBX3,CN=EX1SG1,CN=InformationStore,CN=AUSEX1VS1,CN=Servers,CN=First Administrative Group,CN=Administrative Groups,CN=SETON,CN=Microsoft Exchange,CN=Services,CN=Configuration,DC=seton,DC=org' & _
		'|CN=EX1SG1MBX4,CN=EX1SG1,CN=InformationStore,CN=AUSEX1VS1,CN=Servers,CN=First Administrative Group,CN=Administrative Groups,CN=SETON,CN=Microsoft Exchange,CN=Services,CN=Configuration,DC=seton,DC=org' & _
		'|CN=EX1SG2MBX1,CN=EX1SG2,CN=InformationStore,CN=AUSEX1VS1,CN=Servers,CN=First Administrative Group,CN=Administrative Groups,CN=SETON,CN=Microsoft Exchange,CN=Services,CN=Configuration,DC=seton,DC=org' & _
		'|CN=EX1SG2MBX2,CN=EX1SG2,CN=InformationStore,CN=AUSEX1VS1,CN=Servers,CN=First Administrative Group,CN=Administrative Groups,CN=SETON,CN=Microsoft Exchange,CN=Services,CN=Configuration,DC=seton,DC=org' & _
		'|CN=EX1SG2MBX3,CN=EX1SG2,CN=InformationStore,CN=AUSEX1VS1,CN=Servers,CN=First Administrative Group,CN=Administrative Groups,CN=SETON,CN=Microsoft Exchange,CN=Services,CN=Configuration,DC=seton,DC=org' & _
		'|CN=EX1SG2MBX4,CN=EX1SG2,CN=InformationStore,CN=AUSEX1VS1,CN=Servers,CN=First Administrative Group,CN=Administrative Groups,CN=SETON,CN=Microsoft Exchange,CN=Services,CN=Configuration,DC=seton,DC=org' & _
		'|CN=EX2SG1MBX1,CN=EX2SG1,CN=InformationStore,CN=AUSEX2VS1,CN=Servers,CN=First Administrative Group,CN=Administrative Groups,CN=SETON,CN=Microsoft Exchange,CN=Services,CN=Configuration,DC=seton,DC=org' & _
		'|CN=EX2SG1MBX2,CN=EX2SG1,CN=InformationStore,CN=AUSEX2VS1,CN=Servers,CN=First Administrative Group,CN=Administrative Groups,CN=SETON,CN=Microsoft Exchange,CN=Services,CN=Configuration,DC=seton,DC=org' & _
		'|CN=EX2SG1MBX3,CN=EX2SG1,CN=InformationStore,CN=AUSEX2VS1,CN=Servers,CN=First Administrative Group,CN=Administrative Groups,CN=SETON,CN=Microsoft Exchange,CN=Services,CN=Configuration,DC=seton,DC=org' & _
		'|CN=EX2SG1MBX4,CN=EX2SG1,CN=InformationStore,CN=AUSEX2VS1,CN=Servers,CN=First Administrative Group,CN=Administrative Groups,CN=SETON,CN=Microsoft Exchange,CN=Services,CN=Configuration,DC=seton,DC=org' & _
		'|CN=EX2SG2MBX1,CN=EX2SG2,CN=InformationStore,CN=AUSEX2VS1,CN=Servers,CN=First Administrative Group,CN=Administrative Groups,CN=SETON,CN=Microsoft Exchange,CN=Services,CN=Configuration,DC=seton,DC=org' & _
		'|CN=EX2SG2MBX2,CN=EX2SG2,CN=InformationStore,CN=AUSEX2VS1,CN=Servers,CN=First Administrative Group,CN=Administrative Groups,CN=SETON,CN=Microsoft Exchange,CN=Services,CN=Configuration,DC=seton,DC=org' & _
		'|CN=EX2SG2MBX3,CN=EX2SG2,CN=InformationStore,CN=AUSEX2VS1,CN=Servers,CN=First Administrative Group,CN=Administrative Groups,CN=SETON,CN=Microsoft Exchange,CN=Services,CN=Configuration,DC=seton,DC=org' & _
		'|CN=EX2SG2MBX4,CN=EX2SG2,CN=InformationStore,CN=AUSEX2VS1,CN=Servers,CN=First Administrative Group,CN=Administrative Groups,CN=SETON,CN=Microsoft Exchange,CN=Services,CN=Configuration,DC=seton,DC=org')

$Check_Email = GUICtrlCreateCheckbox("Email", 192, 72, 17, 17)


;List of Accounts and Info
$List_Accounts = GUICtrlCreateListView("", 24, 168, 585, 385)
GUICtrlSetData(-1, "List Accounts")
_GUICtrlListView_AddColumn($List_Accounts, 'Employee ID', (585 / 9) + 8)
_GUICtrlListView_AddColumn($List_Accounts, 'First Name', (585 / 9) + 9)
_GUICtrlListView_AddColumn($List_Accounts, 'Middle', 585 / 9)
_GUICtrlListView_AddColumn($List_Accounts, 'Last Name', (585 / 9) + 9)
_GUICtrlListView_AddColumn($List_Accounts, 'Pin', (585 / 9) - 15)
_GUICtrlListView_AddColumn($List_Accounts, 'Cost Center', 585 / 9)
_GUICtrlListView_AddColumn($List_Accounts, 'Department', 585 / 9)
_GUICtrlListView_AddColumn($List_Accounts, 'Location', 585 / 9)
_GUICtrlListView_AddColumn($List_Accounts, 'SFH', (585 / 9)-15)
GUISetState(@SW_SHOW)
;_Temp()
While 1
	Sleep(2)
WEnd
#EndRegion ### END Koda GUI section ###

;Functions

Func _Load_File()
	$load_File = 0
	$DupeSub = 0
	$Original_file = FileOpenDialog('Enterprise File Location', '\\ausiis01\SHN-FTP-XFR\IS-Psoft\HR-Enterprise-Access', 'TEXT(*.txt)', 3, '', GUICtrlGetHandle($EAI))
	If @error Then
		;MsgBox(4096, "", "No File(s) chosen")
		$fcount = 0
	Else
		FileCopy($Original_file, $Import, 9)
		$oImport = FileOpen($Import, 0)
		$CountLines = _FileCountLines($Import)
		;	MsgBox(0,'',$CountLines)
		$Write_line = _read($oImport, $CountLines)
		$CountLines = ''
		_Write($Write_line)
		FileClose($oImport)
		$aOutput = _Create_array()
		If $aOutput = 0 Then
			MsgBox(0,'Detection Failed', "No ADD's were found in the file")
			Return 0
		EndIf
		Dim $aImported = 0
		;[$x][1] = EmployID; [$x][4] = FirstName; [[$x]5] = MiddleName; [[$x]6] = LastName; [$x][7] = PIN; [$x][9] = CostCenter; [$x][10] = Department; [[$x]11] = Location
		$acleaned = _Cleanup($aOutput)
		Dim $aclean = 0
		Dim $aOutput = 0
	EndIf
	_Fill_ListView($acleaned)
	$load_File = 1
EndFunc   ;==>_Load_File

Func _Temp() ;Used for testing purposes
	Local	$aTemp, $aSplit
	Dim $acleaned[1][9]
	_FileReadToArray(@ScriptDir&'\Test.csv', $aTemp)
	_ArrayDelete($aTemp, 0)
	;_ArrayDisplay($aTemp)
	For $k = 0 To UBound($aTemp) - 1
		ReDim $acleaned[$k + 1][9]
		$aSplit = StringSplit($aTemp[$k],',')
		$acleaned[$k][0] = $aSplit[1]
		$acleaned[$k][1] = $aSplit[2]
		$acleaned[$k][2] = $aSplit[3]
		$acleaned[$k][3] = $aSplit[4]
		$acleaned[$k][4] = $aSplit[5]
		$acleaned[$k][5] = $aSplit[6]
		$acleaned[$k][6] = $aSplit[7]
		$acleaned[$k][7] = $aSplit[8]
		$acleaned[$k][8] = ''
	Next
	;_ArrayDisplay($acleaned)
	;Exit
	_Fill_ListView($acleaned)
	$load_File = 1
EndFunc

Func _Fill_ListView($acleaned)
	$rows = UBound($acleaned, 1)
	For $b = 0 To $rows - 1
		_GUICtrlListView_AddItem($List_Accounts, $acleaned[$b][0])
		_GUICtrlListView_AddSubItem($List_Accounts, $b, $acleaned[$b][1], 1)
		_GUICtrlListView_AddSubItem($List_Accounts, $b, $acleaned[$b][2], 2)
		_GUICtrlListView_AddSubItem($List_Accounts, $b, $acleaned[$b][3], 3)
		_GUICtrlListView_AddSubItem($List_Accounts, $b, $acleaned[$b][4], 4)
		_GUICtrlListView_AddSubItem($List_Accounts, $b, $acleaned[$b][5], 5)
		_GUICtrlListView_AddSubItem($List_Accounts, $b, $acleaned[$b][6], 6)
		_GUICtrlListView_AddSubItem($List_Accounts, $b, $acleaned[$b][7], 7)
		If $acleaned[$b][8] = '001' Then _GUICtrlListView_AddSubItem($List_Accounts, $b, 'Yes', 8)
		If $acleaned[$b][8] <> '001' Then _GUICtrlListView_AddSubItem($List_Accounts, $b, 'No', 8)
	Next
EndFunc   ;==>_Fill_ListView

Func _Close_File()
	_GUICtrlListView_Destroy($List_Accounts)
	$List_Accounts = GUICtrlCreateListView("", 24, 168, 585, 385)
	GUICtrlSetData(-1, "List Accounts")
	_GUICtrlListView_AddColumn($List_Accounts, 'Employee ID', 585 / 9)
	_GUICtrlListView_AddColumn($List_Accounts, 'First Name', 585 / 9)
	_GUICtrlListView_AddColumn($List_Accounts, 'Middle Name', 585 / 9)
	_GUICtrlListView_AddColumn($List_Accounts, 'Last Name', 585 / 9)
	_GUICtrlListView_AddColumn($List_Accounts, 'Pin', (585 / 9) - 3)
	_GUICtrlListView_AddColumn($List_Accounts, 'Cost Center', 585 / 9)
	_GUICtrlListView_AddColumn($List_Accounts, 'Department', 585 / 9)
	_GUICtrlListView_AddColumn($List_Accounts, 'Location', 585 / 9)
	_GUICtrlListView_AddColumn($List_Accounts, 'SFH-Associates', 585 / 9)
	GUISetState(@SW_SHOW)
	$Accounts_Created = 0
	$load_File = 0
EndFunc   ;==>_Close_File

Func _End_Script()
	GUIDelete($EAI)
	Exit
EndFunc   ;==>_End_Script

Func _read($oImport, $LineCount)
	$h = 1
	Do
		$line = FileReadLine($oImport, $h) & @CRLF
		;MsgBox(0,'',$line)
		Local $nLine
		If StringRegExp($line, 'ADD') = 1 Then
			;MsgBox(0,'',$line)
			$nLine = $nLine & $line
			;MsgBox(0,'',$nline)
		EndIf
		$h = $h + 1
		;MsgBox(0,'LineCount',$LineCount)
		;MsgBox(0,'h',$h)
	Until $h = $LineCount + 1

	Return ($nLine)
EndFunc   ;==>_read

Func _Write($line)
	FileDelete($write_File)
	$Iwrite = FileOpen($write_File, 1)
	FileWrite($Iwrite, $line)
	FileClose($Iwrite)
EndFunc   ;==>_Write

Func _Create_array()
	Local $y = 0, $z = 1
	$Iread = FileOpen($write_File, 0)
	$CountLines = _FileCountLines($write_File)
	If $CountLines = 0 Then Return 0
	Dim $aOutput[$CountLines][19]
	;_ArrayDisplay($aOutput)
	;MsgBox(0,'',$CountLines)
	For $x = 1 To $CountLines Step 1
		$i = 0
		$strImported = FileReadLine($Iread, $z)
		;MsgBox(0,'',$x)
		$aImported = StringSplit($strImported, '|')
		_ArrayDelete($aImported, 0)
		;_ArrayDisplay($aImported)
		;FileWrite($log,$aImported[0]&@CRLF)
		;_ArrayDisplay($aOutput)
		If $aImported[0] <> 'ADD' Then
			$CountLines = $CountLines - 1
			ReDim $aImported[$CountLines][19]
		Else
			For $attribute In $aImported
				$aOutput[$y][$i] = $attribute
				$i = $i + 1
			Next
		EndIf
		$y = $y + 1
		$z = $z + 1
	Next
	;_ArrayDisplay($aOutput)
	FileClose($Iread)
	Return $aOutput
EndFunc   ;==>_Create_array

Func _Cleanup($aclean)
	$rows = UBound($aclean, 1)
	;_ArrayDisplay($aclean)
	;MsgBox(0,'',$rows)
	Dim $acleaned[$rows][9]
	For $b = 0 To $rows - 1
		$acleaned[$b][0] = $aclean[$b][1] ;EmployeeID
		$acleaned[$b][1] = $aclean[$b][4] ;Firstname
		$acleaned[$b][2] = $aclean[$b][5] ;Middle Intial
		$acleaned[$b][3] = $aclean[$b][6] ;Last Name
		$acleaned[$b][4] = $aclean[$b][7] ;Pin
		$acleaned[$b][5] = $aclean[$b][9] ;Cost Center
		$acleaned[$b][6] = $aclean[$b][10];Department
		$acleaned[$b][7] = $aclean[$b][11];Location
		$acleaned[$b][8] = $aclean[$b][2] ;SFH
	Next
	;_ArrayDisplay($acleaned)
	Return ($acleaned)
EndFunc   ;==>_Cleanup

Func _Create_CSV()
	If $load_File = 0 Then
		MsgBox(16,'Error','You must first load a PeolplSoft Export file.')
	Else
		_WriteToFileFromArray($finaloutput, $acleaned)
	EndIf
EndFunc   ;==>_Create_CSV

Func _WriteToFileFromArray($File, $a_Array, $i_Base = 0, $i_UBound = 0)
	FileDelete($finaloutput)
	$hFile = FileOpen($finaloutput, 2)
	$rows = UBound($acleaned, 1)
	;MsgBox(0,'',$rows)
	For $b = 0 To $rows - 1
		FileWrite($hFile, $acleaned[$b][0] & ',')
		FileWrite($hFile, $acleaned[$b][1] & ',')
		FileWrite($hFile, $acleaned[$b][2] & ',')
		FileWrite($hFile, $acleaned[$b][3] & ',')
		FileWrite($hFile, $acleaned[$b][4] & ',')
		FileWrite($hFile, $acleaned[$b][5] & ',')
		FileWrite($hFile, $acleaned[$b][6] & ',')
		FileWrite($hFile, $acleaned[$b][7] & ',')
		FileWrite($hFile, @CRLF)
	Next
	FileClose($hFile)
EndFunc   ;==>_WriteToFileFromArray

Func _WriteToFileFromADCreated($File, $a_Array, $i_Base = 0, $i_UBound = 0)
	FileDelete($finaloutput)
	$hFile = FileOpen($finaloutput, 2)
	$rows = UBound($acleaned, 1)
	;MsgBox(0,'',$rows)
	For $b = 0 To $rows - 1
		FileWrite($hFile, $acleaned[$b][0] & ',')
		FileWrite($hFile, $acleaned[$b][1] & ',')
		FileWrite($hFile, $acleaned[$b][2] & ',')
		FileWrite($hFile, $acleaned[$b][3] & ',')
		FileWrite($hFile, $acleaned[$b][4] & ',')
		FileWrite($hFile, $acleaned[$b][5] & ',')
		FileWrite($hFile, $acleaned[$b][6] & ',')
		FileWrite($hFile, $acleaned[$b][7] & ',')
		FileWrite($hFile, @CRLF)
	Next
	FileClose($hFile)
EndFunc   ;==>_WriteToFileFromArray

Func _Import()
	;_ArrayDisplay($acleaned)
	;MsgBox(0,'Wait','Check')
	If $load_File = 0 Then
		MsgBox(16,'Error','You must first load a PeolplSoft Export file.')
	Else
		_Import_Into_AD($acleaned)
	EndIf
EndFunc   ;==>_Import

Func _Import_Into_AD($New_Employees)
	$rows = UBound($New_Employees, 1)
	$ou = 'OU=EnterpriseAccess,OU=User Accounts,DC=seton,DC=org'
	ProgressOn('User Creation', 'User accounts are being created.', 'Please Wait...', Default, Default, 18)
	For $b = 0 To $rows - 1
		ProgressSet(100 * ($b / ($rows - 1)), 'Please Wait...')
		_CreateUser($ou, $New_Employees[$b][0], $New_Employees[$b][1], $New_Employees[$b][2], $New_Employees[$b][3], $New_Employees[$b][4], $New_Employees[$b][5], $New_Employees[$b][6], $New_Employees[$b][7], $New_Employees[$b][8])
		;	Sleep(1000)
	Next
	$Accounts_Created = 1
	ProgressSet(90, 'Import Complete')
	ProgressSet(99, 'Waiting for accounts to finish populating')
	$Number_of_Users = ($rows - $DupeSub)
	If $DupeSub > 0 Then MsgBox(16,'Dupe Count', $DupeSub&" DUPE account(s) detected. Please check logs."&@CRLF)
	$DupeSub = 0
	_Confirm($Xport_List, $ou, '(objectclass=user)')
	ProgressSet(100, 'Account creation complete.')
	Sleep(2000)
	ProgressOff()
EndFunc   ;==>_Import_Into_AD

Func _CreateUser($userou, $employID, $fname, $mname, $lname, $pin, $costcener, $department, $location, $SFH_Associates)
	Local $Dupe = ''
	$RSearch = _DuplicateSearch('EmployeeID','PinNumber',$employID,$pin,'displayName','displayName')

	$fnsearch = StringInStr($RSearch,$fname)
	$lnsearch = StringInStr($RSearch,$lname)

	If $fnsearch <> 0 And $lnsearch <> 0 Then
		;MsgBox(0,'DUPE Deteced',$lname&', '&$fname&' is a duplicate account.')
		$oLog = FileOpen($log, 1)
		FileWrite($oLog,'DUPE account detected: EmployeeID='&$employID&' PIN='&$pin&' DisplayName='&$lname&', '&$fname&@CRLF)
		FileClose($oLog)
		$Dupe = 1
		$DupeSub = $DupeSub + 1
		_Update_Entry('EmployeeID','PinNumber',$employID,$pin,'displayName','displayName', 1, $fname, $lname, $location, $SFH_Associates)
	Else
	;  MyErr

		$user = StringLower(StringLeft($fname, 1)) & StringLower(StringLeft($mname, 1)) & $lname

		$displayname = _DeterminDisplayName($fname, $mname, $lname)
		;MsgBox(0,'Display Name',$displayname)
		If _ADObjectExists($user) Then
			Local $InUse
			$InUse = $user
			Local $x = 1
			Do
				$user = StringLower(StringLeft($fname, 1)) & StringLower(StringLeft($mname, 1)) & $lname & $x
				$x = $x + 1
				$return = _ADObjectExists($user)
			Until $return = 0
			$userou= 'OU=Suspected Dupes,OU=EnterpriseAccess,OU=User Accounts,DC=seton,DC=org'
			$oLog = FileOpen($log, 1)
			FileWrite($oLog,'User name '&$InUse&' already exist but not identified as a DUPE. Used different username of '&$user&' for '&$displayname&'.'&@CRLF)
			FileClose($oLog)
		EndIf
		$ObjOU = ObjGet("LDAP://" & $strHostServer & "/" & $userou)
		If $mname = "" Then
			$cnname = "CN=" & $lname & "\, " & $fname
		Else
			$cnname = "CN=" & $lname & "\, " & $fname & " " & StringLeft($mname, 1) & '.'
		EndIf
		;MsgBox(0,'cn',$cnname)
		;Remove any apostraphies from name for email.
		$user = StringReplace($user,"'",'')
		$user = StringReplace($user,"-",'')
		$user = StringStripWS($user,8)

		;Create the user
		$ObjUser = $ObjOU.Create("User", $cnname)
		$ObjUser.Put("sAMAccountName", $user)
		$ObjUser.Put("employeeID", $employID)
		$ObjUser.Put("pinNumber", $pin)
		$ObjUser.Put("Department", $costcener & ' - ' & $department)
		$ObjUser.Put("physicalDeliveryOfficeName", $location)
		$ObjUser.Put("company", 'TXAUS')
		$objUser.Put("description", 'EnterPrise Access Created')
		;$ObjUser.Put ("primaryGroupID", '513')
		$ObjUser.Put("sn", $lname)
		$ObjUser.Put("givenName", $fname)
		If $mname <>'' Then $ObjUser.Put("initials", StringLeft($mname, 1))
		$ObjUser.Put("displayName", $displayname)
		$ObjUser.Put("UserPrincipalName", $user & '@seton.org')
		$ObjUser.SetInfo
		;After creating the user we can now set the password and force a change at logon
		$ObjUser.Put("homeDirectory", '\\seton\homedir\users\' & $user)
		$ObjUser.Put("homeDrive", 'H:')
		$ObjUser.SetPassword(GUICtrlRead($Password_Input))
		$ObjUser.Put("PwdLastSet", '0')
		$ObjUser.Put("userAccountControl", '512')
		$ObjUser.SetInfo
		;Create all necessary groups
		_ADAddUserToGroup(_ADSamAccountNameToFQDN('Home00050'), _ADSamAccountNameToFQDN($user))
		If $SFH_Associates = '001' Then _ADAddUserToGroup(_ADSamAccountNameToFQDN('SFH-Associates'), _ADSamAccountNameToFQDN($user))
		If StringInStr($location,"Dell Children's") Then _ADAddUserToGroup(_ADSamAccountNameToFQDN('All-DCMC'), _ADSamAccountNameToFQDN($user))
		If StringInStr($location,"Chevy") Then _ADAddUserToGroup(_ADSamAccountNameToFQDN('All-Chevy'), _ADSamAccountNameToFQDN($user))
		If StringInStr($location,"Brack") Then _ADAddUserToGroup(_ADSamAccountNameToFQDN('All-BH'), _ADSamAccountNameToFQDN($user))
		If StringInStr($location,"5555") Then _ADAddUserToGroup(_ADSamAccountNameToFQDN('All-5555'), _ADSamAccountNameToFQDN($user))
		If StringInStr($location,"4200") Then _ADAddUserToGroup(_ADSamAccountNameToFQDN('All-4200'), _ADSamAccountNameToFQDN($user))
		If StringInStr($location,"1005") Then _ADAddUserToGroup(_ADSamAccountNameToFQDN('All-1005'), _ADSamAccountNameToFQDN($user))
		If StringInStr($location,"Williamson") Then _ADAddUserToGroup(_ADSamAccountNameToFQDN('All-SMCW'), _ADSamAccountNameToFQDN($user))
		If StringInStr($location,"Austin") Then _ADAddUserToGroup(_ADSamAccountNameToFQDN('All-SMC'), _ADSamAccountNameToFQDN($user))
		If StringInStr($location,"Highland") Then _ADAddUserToGroup(_ADSamAccountNameToFQDN('All-SHLMC'), _ADSamAccountNameToFQDN($user))
		If StringInStr($location,"Edgar") Then _ADAddUserToGroup(_ADSamAccountNameToFQDN('All-SEBD'), _ADSamAccountNameToFQDN($user))
		If StringInStr($location,"Rio") Then _ADAddUserToGroup(_ADSamAccountNameToFQDN('All-RG'), _ADSamAccountNameToFQDN($user))
		If StringInStr($location,"Jefferson") Then _ADAddUserToGroup(_ADSamAccountNameToFQDN('All-Jefferson'), _ADSamAccountNameToFQDN($user))
		If StringInStr($location,"Southwest") Then _ADAddUserToGroup(_ADSamAccountNameToFQDN('All-SSW'), _ADSamAccountNameToFQDN($user))
		If StringInStr($location,"Shoal") Then _ADAddUserToGroup(_ADSamAccountNameToFQDN('All-SSC'), _ADSamAccountNameToFQDN($user))
		If StringInStr($location,"Northwest") Then _ADAddUserToGroup(_ADSamAccountNameToFQDN('All-SNW'), _ADSamAccountNameToFQDN($user))
		If StringInStr($location,"Clinical Educat") Then
			_ADAddUserToGroup(_ADSamAccountNameToFQDN('All-CEC'), _ADSamAccountNameToFQDN($user))
			_ADAddUserToGroup(_ADSamAccountNameToFQDN('All-BH'), _ADSamAccountNameToFQDN($user))
		EndIf
		;create users H:\ Drive
		DirCreate('\\seton\homedir\users\' & $user)
		$acls = ('cacls.exe \\seton\homedir\users\'&$user&' /E /T /C /G "' & $user & '@seton":F')
		;MsgBox(0,'',$acls)
		Run(@ComSpec & " /C " & $acls, "", @SW_HIDE);_RunDOS($acls)
		$acls = ('cacls.exe \\seton\homedir\users\'&$user&' /E /T /C /G "' & 'OU Admins' & '@seton":F')
		;MsgBox(0,'',$acls)
		Run(@ComSpec & " /C " & $acls, "", @SW_HIDE);_RunDOS($acls)
		;MsgBox(0,'Check Box',GUICtrlRead($Check_Email))
		If GUICtrlRead($Check_Email) = 1 Then
		$mail = _EX($user)
		;FileWrite($log,$user)
		$oLog = FileOpen($log, 1)
		If $mail = 0 Then FileWrite($oLog,'There was a problem creating a mailbox for '&$user&'.')
		FileClose($oLog)
		EndIf
	EndIf
EndFunc   ;==>_CreateUser

Func _Update_Entry($search_object1, $search_object2, $object1, $object2, $attribute1, $attribute2, $Single, $fname, $lname, $location, $SFH_Associates)
	Local $aDisplayNames[1], $y, $SAM, $ID, $aGroups, $CreatedEMail, $ActivatedUsers, $UpdatedIDs
	$CreatedEMail = @ScriptDir&'\Existing Users Email Creation.log'
	$ActivatedUsers = @ScriptDir&'\Existing Users Activated.log'
	$UpdatedIDs = @ScriptDir&'\Existing Update EmployeeIDs.log'

	;Local $aresult[1]
	;_ArrayDisplay($aresult)
	;MsgBox(0,'','')
	$x = 1
	$result = ""
	$strQuery = "<LDAP://" & $strHostServer & "/" & $strDNSDomain & ">;("&$search_object1&"=" & $object1 & ");ADsPath;subtree"
	$objRecordSet = $objConnection.Execute($strQuery) ; Retrieve the FQDN for the object
	$ldap_entry = $objRecordSet.fields(0).value
	If $ldap_entry = '' Then
	Else
		$oObject = ObjGet($ldap_entry) ; Retrieve the COM Object for the object
		If Not IsObj($oObject) Then
			;skip
		Else
			$result = $oObject.Get($attribute1)
			$oObject.PurgePropertyList
			$oObject = 0
		EndIf
	EndIf

	$strQuery = "<LDAP://" & $strHostServer & "/" & $strDNSDomain & ">;("&$search_object2&"=" & $object2 & ");ADsPath;subtree"
	$objRecordSet = $objConnection.Execute($strQuery) ; Retrieve the FQDN for the object
	$objRecordSet.MoveFirst
	$y = 0
	Do
		$ldap_entry = $objRecordSet.fields(0).value
		If $ldap_entry = '' Then
		Else
			$oObject = ObjGet($ldap_entry) ; Retrieve the COM Object for the object
			If Not IsObj($oObject) Then
				;skip
			Else
				ReDim $aDisplayNames[$y + 1]
				$aDisplayNames[$y] = $oObject.Get($attribute2)
				;MsgBox(0,'',$result)
				$oObject.PurgePropertyList
				$y = $y + 1
			EndIf
		EndIf
		$objRecordSet.MoveNext
	Until $objRecordSet.EOF
	$oObject = 0
	$objRecordSet = 0

	;_ArrayDisplay($aDisplayNames)

	For $Entry In $aDisplayNames
		If StringInStr($Entry,$fname) And StringInStr($Entry,$lname) Then
			$SAM = _AttributeSearch('displayName', $Entry, 'samAccountName')
			If _ADGetObjectAttribute($SAM,'userAccountControl') = '514' Then

				_ADGetUserGroups($aGroups, $SAM)
				_ArrayDelete($aGroups, 0)
				For $Group In $aGroups
					$Return = _ADRemoveUserFromGroup($Group, _ADSamAccountNameToFQDN($sam))
				Next

				$FQDN = _ADSamAccountNameToFQDN($SAM)
				$sSourceCN = 'LDAP://'&$FQDN
				$sDestCN = "LDAP://OU=Users,OU=User Accounts," & $strDNSDomain

				$objOU = ObjGet($sDestCN)
				If Not IsObj($objOU) Then
					MsgBox(16,'User Object Error','Error getting user object.', 3)
					FileWriteLine($ActivatedUsers, 'Error '&$Entry&"'s account was not successfully reactivated.")
				Else
					$objUser = $objOU.MoveHere($sSourceCN, 'CN='&$SAM)
					_ADModifyAttribute($SAM, "userAccountControl", '512')
					FileWriteLine($ActivatedUsers, $Entry&"'s account was reactivated. All previous memberships have been removed.")
				EndIf
				$ID = _ADGetObjectAttribute($SAM,'employeeID')
				If $ID <> $object1 Then
					_ADModifyAttribute($SAM, 'employeeID', $object1)
					FileWriteLine($UpdatedIDs,$Entry&"'s ID was changed from "&'"'&$ID&'" to "'&$object1&'".')
				EndIf
			EndIf
			If GUICtrlRead($Check_Email) = 1 Then $mail = _EX($SAM)
			If $mail = 1 Then
				FileWriteLine($CreatedEMail,'An email account was created for '&$Entry&'.')
			Else
				FileWriteLine($CreatedEMail,'An email account was NOT created for '&$Entry&'.')
			EndIf
			_ADAddUserToGroup(_ADSamAccountNameToFQDN('Home00050'), _ADSamAccountNameToFQDN($SAM))
			If $SFH_Associates = '001' Then _ADAddUserToGroup(_ADSamAccountNameToFQDN('SFH-Associates'), _ADSamAccountNameToFQDN($SAM))
			If StringInStr($location,"Dell Children's") Then _ADAddUserToGroup(_ADSamAccountNameToFQDN('All-DCMC'), _ADSamAccountNameToFQDN($SAM))
			If StringInStr($location,"Chevy") Then _ADAddUserToGroup(_ADSamAccountNameToFQDN('All-Chevy'), _ADSamAccountNameToFQDN($SAM))
			If StringInStr($location,"Brack") Then _ADAddUserToGroup(_ADSamAccountNameToFQDN('All-BH'), _ADSamAccountNameToFQDN($SAM))
			If StringInStr($location,"5555") Then _ADAddUserToGroup(_ADSamAccountNameToFQDN('All-5555'), _ADSamAccountNameToFQDN($SAM))
			If StringInStr($location,"4200") Then _ADAddUserToGroup(_ADSamAccountNameToFQDN('All-4200'), _ADSamAccountNameToFQDN($SAM))
			If StringInStr($location,"1005") Then _ADAddUserToGroup(_ADSamAccountNameToFQDN('All-1005'), _ADSamAccountNameToFQDN($SAM))
			If StringInStr($location,"Williamson") Then _ADAddUserToGroup(_ADSamAccountNameToFQDN('All-SMCW'), _ADSamAccountNameToFQDN($SAM))
			If StringInStr($location,"Austin") Then _ADAddUserToGroup(_ADSamAccountNameToFQDN('All-SMC'), _ADSamAccountNameToFQDN($SAM))
			If StringInStr($location,"Highland") Then _ADAddUserToGroup(_ADSamAccountNameToFQDN('All-SHLMC'), _ADSamAccountNameToFQDN($SAM))
			If StringInStr($location,"Edgar") Then _ADAddUserToGroup(_ADSamAccountNameToFQDN('All-SEBD'), _ADSamAccountNameToFQDN($SAM))
			If StringInStr($location,"Rio") Then _ADAddUserToGroup(_ADSamAccountNameToFQDN('All-RG'), _ADSamAccountNameToFQDN($SAM))
			If StringInStr($location,"Jefferson") Then _ADAddUserToGroup(_ADSamAccountNameToFQDN('All-Jefferson'), _ADSamAccountNameToFQDN($SAM))
			If StringInStr($location,"Southwest") Then _ADAddUserToGroup(_ADSamAccountNameToFQDN('All-SSW'), _ADSamAccountNameToFQDN($SAM))
			If StringInStr($location,"Shoal") Then _ADAddUserToGroup(_ADSamAccountNameToFQDN('All-SSC'), _ADSamAccountNameToFQDN($SAM))
			If StringInStr($location,"Northwest") Then _ADAddUserToGroup(_ADSamAccountNameToFQDN('All-SNW'), _ADSamAccountNameToFQDN($SAM))
		EndIf
	Next

EndFunc

Func _Add_Users_To_A_Group()
	If $load_File = 0 Then
		MsgBox(16,'Error','You must first load a PeolplSoft Export file.')
	Else
		_List_to_Group($acleaned)
	EndIf
EndFunc   ;==>_Add_Users_To_A_Group

Func _List_to_Group($New_Employees)
	$group = InputBox('Group to be joined to', 'Please input the name of the group you wish to add the users to.' & @CRLF & @CRLF & 'Ex: All-SMC', 'Type Here')
	$rows = UBound($New_Employees, 1)
	ProgressOn('Group Adding', 'User accounts are being added to ' & $group & '.', 'Please Wait...', Default, Default, 18)
	$GroupFQDN = _ADSamAccountNameToFQDN($group)
	For $b = 0 To $rows - 1
		ProgressSet(100 * ($b / ($rows - 1)), 'Please Wait...')
		;MsgBox(0,'Firstname',$New_Employees[$b][1])
		;MsgBox(0,'MiddleName',$New_Employees[$b][2])
		;MsgBox(0,'LastName',$New_Employees[$b][3])
		$displayname = _DeterminDisplayName($New_Employees[$b][1], $New_Employees[$b][2], $New_Employees[$b][3])
		;MsgBox(0,'DisplayName',$displayname)
		$UserFQDN = _DisplayNameToFQDN($displayname)
		If $UserFQDN = '' Then
			$oLog = FileOpen($log, 1)
			FileWrite($oLog, 'User ' & $displayname & "'s Fully Qualified Domain Name could not be found." & @CRLF)
			$AddError = 1
			FileClose($oLog)
		Else
			;MsgBox(0,'FQDN',$FQDNname)
			_ADAddUserToGroup($GroupFQDN, $UserFQDN)
		EndIf
	Next
	If $AddError = 1 Then MsgBox(0, 'Error', 'There was an account that could not be added. Please see log at ' & $log & '.')
	$AddError = 0
	ProgressSet(100, 'Adding Complete')
	Sleep(3000)
	ProgressOff()
EndFunc   ;==>_List_to_Group

Func _DeterminDisplayName($fname, $mname, $lname)

	If $mname = '' Then
		$displayname = $lname & ', ' & $fname
	Else
		$displayname = $lname & ', ' & $fname & ' ' & StringLeft($mname, 1) & '.'
	EndIf
	Return $displayname
EndFunc   ;==>_DeterminDisplayName

Func _DisplayNameToFQDN($displayname)

	$strQuery = "<LDAP://" & $strHostServer & "/" & $strDNSDomain & ">;(DisplayName=" & $displayname & ");distinguishedName;subtree"
	$objRecordSet = $objConnection.Execute($strQuery)

	If $objRecordSet.RecordCount = 1 Then
		$fqdn = $objRecordSet.fields(0).value
		$objRecordSet = 0
		Return StringReplace($fqdn, "/", "\/")
	Else
		$objRecordSet = 0
		Return ""
	EndIf
EndFunc   ;==>_DisplayNameToFQDN

Func ADGetObjectsInOUwithAttrib(ByRef $aArray, $ou, $filter2 = '', $filter = "(name=*)", $searchscope = 2, $datatoretrieve = "sAMAccountName", $datatoretrieve2 = "Name", $sortby = "sAMAccountName")
	Dim $ObjectArray, $ObjectArray2
	Local $objRecordSet
	$objCommand = ObjCreate("ADODB.Command")
	$objCommand.ActiveConnection = $objConnection
	$objCommand.Properties("Page Size") = 256
	$objCommand.Properties("Searchscope") = $searchscope
	$objCommand.Properties("TimeOut") = 20

	If $filter2 = '' Then
		$strCmdText = "<LDAP://" & $strHostServer & "/" & $ou & ">;" & $filter & ";" & $datatoretrieve & ";subtree"
	Else
		$strCmdText = "<LDAP://" & $strHostServer & "/" & $ou & ">;" & '(&' & $filter2 & $filter & ')' & ";" & $datatoretrieve & ";subtree"
	EndIf

	If $filter2 = '' Then
		$strCmdText2 = "<LDAP://" & $strHostServer & "/" & $ou & ">;" & $filter & ";" & $datatoretrieve2 & ";subtree"
	Else
		$strCmdText2 = "<LDAP://" & $strHostServer & "/" & $ou & ">;" & '(&' & $filter2 & $filter & ')' & ";" & $datatoretrieve2 & ";subtree"
	EndIf

	$objCommand.CommandText = $strCmdText

	$objRecordSet = $objCommand.Execute

	$recordcount = $objRecordSet.RecordCount

	If $recordcount = 0 Then
		$objCommand = 0
		$objRecordSet = 0
		Return 0
	EndIf

	If StringInStr($datatoretrieve, ",") Then

		$dtrArray = StringSplit($datatoretrieve, ",")

		Dim $ObjectArray[$recordcount + 1][$dtrArray[0]]

		$ObjectArray[0][0] = $recordcount
		$ObjectArray[0][1] = $dtrArray[0]

		$count = 1
		$objRecordSet.MoveFirst
		Do
			For $i = 1 To $dtrArray[0]
				$ObjectArray[$count][$i - 1] = $objRecordSet.Fields($dtrArray[$i] ).Value
			Next

			$objRecordSet.MoveNext
			$count += 1
		Until $objRecordSet.EOF
	Else
		Dim $ObjectArray[$recordcount + 1]
		$ObjectArray[0] = UBound($ObjectArray) - 1
		If $ObjectArray[0] = 0 Then
			$ObjectArray = 0
			Return 0
		Else
			$count = 1
			$objRecordSet.MoveFirst
			Do
				$ObjectArray[$count] = $objRecordSet.Fields($datatoretrieve).Value
				$objRecordSet.MoveNext
				$count += 1
			Until $objRecordSet.EOF
		EndIf
	EndIf

	$objRecordSet = 0

	$objCommand.CommandText = $strCmdText2

	$objRecordSet = $objCommand.Execute

	$recordcount = $objRecordSet.RecordCount
	If $recordcount = 0 Then
		$objCommand = 0
		$objRecordSet = 0
		Return 0
	EndIf

	If StringInStr($datatoretrieve, ",") Then

		$dtrArray = StringSplit($datatoretrieve, ",")

		Dim $ObjectArray2[$recordcount + 1][$dtrArray[0]]

		$ObjectArray2[0][0] = $recordcount
		$ObjectArray2[0][1] = $dtrArray[0]

		$count = 1
		$objRecordSet.MoveFirst
		Do
			For $i = 1 To $dtrArray[0]
				$ObjectArray2[$count][$i - 1] = $objRecordSet.Fields($dtrArray[$i] ).Value
			Next

			$objRecordSet.MoveNext
			$count += 1
		Until $objRecordSet.EOF
	Else
		Dim $ObjectArray2[$recordcount + 1]
		$ObjectArray2[0] = UBound($ObjectArray) - 1
		If $ObjectArray2[0] = 0 Then
			$ObjectArray2 = 0
			Return 0
		Else
			$count = 1
			$objRecordSet.MoveFirst
			Do
				$ObjectArray2[$count] = $objRecordSet.Fields($datatoretrieve2).Value
				$objRecordSet.MoveNext
				$count += 1
			Until $objRecordSet.EOF
		EndIf
	EndIf

	$objCommand = 0
	$objRecordSet = 0
	_ArrayDelete($ObjectArray, 0)
	_ArrayDelete($ObjectArray2, 0)
	$rows = UBound($ObjectArray, 1)
	Dim $Xport_List[$rows][2]

	Local $y = 0
	For $x = 0 To $rows - 1
		$username = $ObjectArray[$y]
		$Full_Name = $ObjectArray2[$y]
		$Xport_List[$y][0] = '"' & $Full_Name & '"'
		$Xport_List[$y][1] = $username
		$y = $y + 1
	Next
EndFunc   ;==>ADGetObjectsInOUwithAttrib

Func _Create_array_from_file()
	Local $y = 0, $z = 1
	$adRead = FileOpen($AD_Export, 0)
	$CountLines = _FileCountLines($AD_Export)
	;MsgBox(0,'',$CountLines)
	Dim $accounts[$CountLines][2]
	;MsgBox(0,'',$CountLines)
	For $x = 1 To $CountLines Step 1
		$i = 0
		$strImported = FileReadLine($adRead, $z)
		;MsgBox(0,'',$strImported)
		;MsgBox(0,'',$x)
		$aImported = StringSplit($strImported, Chr(9))
		_ArrayDelete($aImported, 0)
		;_ArrayDisplay($aImported)
		;FileWrite($log,$aImported[0]&@CRLF)
		;_ArrayDisplay($accounts)
		For $elements In $aImported
			$accounts[$y][$i] = $elements
			$i = $i + 1
		Next
		$y = $y + 1
		$z = $z + 1
	Next
	;_ArrayDisplay($accounts)
	FileClose($adRead)
	Dim $y = 0, $z = 1
	$rows = UBound($accounts, 1)
	Dim $SSO_Import[$rows][3]
	For $x = 0 To $rows - 2
		$SSO_Import[$z][0] = StringReplace($accounts[$z][1], '@seton.org', '')
		$lastname = _StringBetween($accounts[$z][0], '"', ',')
		$SSO_Import[$z][1] = $lastname[0]
		$firstname = _StringBetween($accounts[$z][0], ', ', '"')
		$SSO_Import[$z][2] = $firstname[0]
		;$SSO_Import[$z][3] = 'windows'
		;$SSO_Import[$z][3] = $SSO_Import[$z][0]
		;$SSO_Import[$z][5] = 'role'
		;$SSO_Import[$z][6] = 'clinician'
		$z = $z + 1
	Next
	_ArrayDelete($SSO_Import, 0)
	Dim $accounts = 0
	Return $SSO_Import
EndFunc   ;==>_Create_array_from_file

Func _Create_array_from_array($accounts)
	Dim $y = 0, $z = 0
	$rows = UBound($accounts, 1)
	;MsgBox(0,'',$rows)
	Dim $SSO_Import[$rows + 1][3]
	For $x = 0 To $rows - 1
		$SSO_Import[$z][0] = $accounts[$z][1]
		$lastname = _StringBetween($accounts[$z][0], '"', ',')
		$SSO_Import[$z][1] = $lastname[0]
		$firstname = _StringBetween($accounts[$z][0], ', ', '"')
		$SSO_Import[$z][2] = $firstname[0]
		$z = $z + 1
	Next
	_ArrayDelete($SSO_Import, UBound($SSO_Import, 1))
	Return $SSO_Import
EndFunc   ;==>_Create_array_from_array

Func _EnterpriseAccess_CreateSSOXML()
	If $load_File = 0 Then
		MsgBox(16,'Error','You must first load a PeolplSoft Export file.')
	Else
		ADGetObjectsInOUwithAttrib($Xport_List, $ou, '(objectclass=user)')
		;$names = _Create_array_from_file()
		;_ArrayDisplay($Xport_List) ; Display name and username
		$names = _Create_array_from_array($Xport_List)
		;_ArrayDisplay($names)
		Dim $accounts = 0
		_Create_XML($names)
		MsgBox(0,'Done','The XML file has been created.')
	EndIf
EndFunc   ;==>_EnterpriseAccess_CreateSSOXML

Func _Create_XML($names)
	FileDelete($XML)
	$writeToXML = FileOpen($XML, 2)
	FileWriteLine($writeToXML, '<?xml version="1.0" encoding="ASCII"?><userList' & @CRLF)
	FileWriteLine($writeToXML, 'xmlns="http://www.sentillion.com"' & @CRLF)
	FileWriteLine($writeToXML, 'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"' & @CRLF)
	FileWriteLine($writeToXML, 'xsi:schemaLocation="http://www.sentillion.com uma.xsd">' & @CRLF)
	;start user loop
	Local $z = 0
	$rows = UBound($names, 1)
	For $x = 0 To $rows - 1
		Local $username = $names[$z][0], $lastname = $names[$z][1], $firstname = $names[$z][2]
		FileWriteLine($writeToXML, '<user><uid>' & $username & '</uid><sn>' & $lastname & '</sn><givenName> ' & $firstname & '</givenName>' & @CRLF)
		FileWriteLine($writeToXML, '<appEntryList>' & @CRLF)
		FileWriteLine($writeToXML, '<appEntry><logonSuffix>windows</logonSuffix><logonId>' & $username & '</logonId><password></password></appEntry>' & @CRLF)
		FileWriteLine($writeToXML, '<appEntry><logonSuffix>role</logonSuffix><logonId>clinician</logonId><password></password></appEntry>' & @CRLF)
		FileWriteLine($writeToXML, '</appEntryList></user>' & @CRLF)
		$z = $z + 1
	Next
	FileWriteLine($writeToXML, '</userList>' & @CRLF)
	FileClose($writeToXML)
EndFunc   ;==>_Create_XML

Func _Confirm(ByRef $aArray, $ou, $filter2 = '', $filter = "(name=*)", $searchscope = 2, $datatoretrieve = "sAMAccountName", $datatoretrieve2 = "Name", $sortby = "sAMAccountName")
	Dim $ObjectArray, $ObjectArray2
	Local $objRecordSet
	$objCommand = ObjCreate("ADODB.Command")
	$objCommand.ActiveConnection = $objConnection
	$objCommand.Properties("Page Size") = 256
	$objCommand.Properties("Searchscope") = $searchscope
	$objCommand.Properties("TimeOut") = 20

	If $filter2 = '' Then
		$strCmdText = "<LDAP://" & $strHostServer & "/" & $ou & ">;" & $filter & ";" & $datatoretrieve & ";subtree"
	Else
		$strCmdText = "<LDAP://" & $strHostServer & "/" & $ou & ">;" & '(&' & $filter2 & $filter & ')' & ";" & $datatoretrieve & ";subtree"
	EndIf

	If $filter2 = '' Then
		$strCmdText2 = "<LDAP://" & $strHostServer & "/" & $ou & ">;" & $filter & ";" & $datatoretrieve2 & ";subtree"
	Else
		$strCmdText2 = "<LDAP://" & $strHostServer & "/" & $ou & ">;" & '(&' & $filter2 & $filter & ')' & ";" & $datatoretrieve2 & ";subtree"
	EndIf

	$objCommand.CommandText = $strCmdText

	Do
		$objRecordSet = $objCommand.Execute

		$recordcount = $objRecordSet.RecordCount
		If $recordcount < $Number_of_Users Then Sleep(10000)
		$Timer = $Timer + 1
	Until $recordcount = $Number_of_Users Or $Timer = 12

	If $Timer = 12 Then $Error = MsgBox(16, 'Warning', 'After waiting 2 minutes there were ' & $recordcount & ' account(s) found in the Enterprise Access OU but, ' & $Number_of_Users & ' account(s) should have been created.')

	$objRecordSet = 0

EndFunc   ;==>_Confirm

Func _EX($user)
	If _ADObjectExists($user) = 0 Then Return 0
	;MsgBox(0,'Found','Found User account')
	$strQuery = "<LDAP://" & $strHostServer & "/" & $strDNSDomain & ">;(sAMAccountName=" & $user & ");ADsPath;subtree"
	$objRecordSet = $objConnection.Execute($strQuery) ; Retrieve the FQDN for the group, if it exists
	$ldap_entry = $objRecordSet.fields(0).value
	$oUser = ObjGet($ldap_entry) ; Retrieve the COM Object for the object

	If $oUser.HomeMDB <> "" Then Return 0
	;MsgBox(0,'Server',"LDAP://"&GUICtrlRead($EXSvr))
	$oUser.CreateMailbox("LDAP://" & GUICtrlRead($EXSvr))
	$oUser.SetInfo
	$oUser.Put("msExchOmaAdminWirelessEnable", "7")
	$oUser.setinfo
	$oUser = 0
	Return 1
EndFunc   ;==>_EX

Func _Export_AD_Info()
	_Create_User_AD_Export()
EndFunc

Func _Create_User_AD_Export()
	;If $Accounts_Created = 0 Then
	;	MsgBox(16,'Error','You must first create the users. If you have make sure they are populated in the Enterprise Access Folder.')
	;Else
		ADGetObjectsInOUwithAttrib($Xport_List, $ou, '(objectclass=user)')
		;MsgBox (0,'',$Xport_List[0][1])
		;_ArrayDisplay($Xport_List)
		Local $rows = UBound($Xport_List,1)
		Dim $Users [$rows]
		For $b = 0 To $rows-1
			$Users[$b] = $Xport_List[$b][1]
		Next
		;_ArrayDisplay($Users)
		$EmployInfo = _ADGetEmployeesAttribute($Users,'EmployeeID','displayname', 'sAMAccountName',1)
		_ArrayDisplay($EmployInfo)
		FileDelete($User_Info_Export)
	$hFile = FileOpen($User_Info_Export, 2)
	FileWrite($hFile,'EmployeeID,Last Name,First Name,Network ID,Email'&@CRLF)
	For $b = 1 To $EmployInfo[0]
		FileWrite($hFile, $EmployInfo[$b] & @CRLF)
	Next
	FileClose($hFile)
	;EndIf
EndFunc

Func _ADGetEmployeesAttribute($object, $attribute, $attribute2 = '', $attribute3 = '',$Single = 0)
	If $Single = 0 Then
		If _ADObjectExists($object) = 0 Then Return 0
		$strQuery = "<LDAP://" & $strHostServer & "/" & $strDNSDomain & ">;(sAMAccountName=" & $object & ");ADsPath;subtree"
		$objRecordSet = $objConnection.Execute($strQuery) ; Retrieve the FQDN for the object
		$ldap_entry = $objRecordSet.fields(0).value
		$oObject = ObjGet($ldap_entry) ; Retrieve the COM Object for the object
		$result = $oObject.Get($attribute)
		$oObject.PurgePropertyList
		$oObject = 0
		If $result = "" Then
			Return ""
		Else
			Return $result
		EndIf
	ElseIf $Single = 1 Then
		Local $aresult[UBound($object) + 1]
		$x = 1
		For $accounts In $object
			If _ADObjectExists($accounts) = 0 Then
				$resulta = ""
				$resultb = ""
				$resultc = ""
			Else
				$strQuery = "<LDAP://" & $strHostServer & "/" & $strDNSDomain & ">;(sAMAccountName=" & $accounts & ");ADsPath;subtree"
				$objRecordSet = $objConnection.Execute($strQuery) ; Retrieve the FQDN for the object
				$ldap_entry = $objRecordSet.fields(0).value
				$oObject = ObjGet($ldap_entry) ; Retrieve the COM Object for the object
				$resulta = $oObject.Get($attribute)
				$resultb = $oObject.Get($attribute2)
				$resultc = $oObject.Get($attribute3)
				$oObject.PurgePropertyList
				$oObject = 0
			EndIf
			If $resulta = "" And $resultb = "" And $resultc = "" Then
				;Skip
			Else
				$aresult[$x] = $resulta & ',' & $resultb & ',' & $resultc & ',' & $resultc&'@seton.org'
			EndIf
			$x = $x + 1
		Next
		$aresult[0] = UBound($aresult) - 1
		Return $aresult
	Else
		Return 'Single parameter unknown'
	EndIf

EndFunc

Func _DuplicateSearch($search_object1, $search_object2, $object1, $object2, $attribute1, $attribute2,$Single = 1)

		;Local $aresult[1]
		;_ArrayDisplay($aresult)
		;MsgBox(0,'','')
		$x = 1
		$result = ""
		$strQuery = "<LDAP://" & $strHostServer & "/" & $strDNSDomain & ">;("&$search_object1&"=" & $object1 & ");ADsPath;subtree"
		$objRecordSet = $objConnection.Execute($strQuery) ; Retrieve the FQDN for the object
		$ldap_entry = $objRecordSet.fields(0).value
		If $ldap_entry = '' Then
		Else
			$oObject = ObjGet($ldap_entry) ; Retrieve the COM Object for the object
			If Not IsObj($oObject) Then
				;skip
			Else
				$result = $oObject.Get($attribute1)
				$oObject.PurgePropertyList
				$oObject = 0
			EndIf
		EndIf

		$strQuery = "<LDAP://" & $strHostServer & "/" & $strDNSDomain & ">;("&$search_object2&"=" & $object2 & ");ADsPath;subtree"
		$objRecordSet = $objConnection.Execute($strQuery) ; Retrieve the FQDN for the object
		$objRecordSet.MoveFirst
		Do
			$ldap_entry = $objRecordSet.fields(0).value
			If $ldap_entry = '' Then
			Else
				$oObject = ObjGet($ldap_entry) ; Retrieve the COM Object for the object
				If Not IsObj($oObject) Then
					;skip
				Else
					If $result = '' Then
						$result = $oObject.Get($attribute2)
					Else
						;MsgBox(0,'',$result)
						$result = $result&','&$oObject.Get($attribute2)
					EndIf
					;MsgBox(0,'',$result)
					$oObject.PurgePropertyList
				EndIf
			EndIf
			$objRecordSet.MoveNext
		Until $objRecordSet.EOF
		$oObject = 0
		$objRecordSet = 0

		If $result = "" Then
			;Skip
		Else
			;$aresult[0] = $result
			;_ArrayDisplay($aresult)
		EndIf
		Return $result

EndFunc

