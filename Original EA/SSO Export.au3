#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Outfile=SSO Export.exe
#AutoIt3Wrapper_Res_requestedExecutionLevel=asInvoker
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****


#include <AD-original.au3>
#include <String.au3>

Global	$Xport_List

_EnterpriseAccess_CreateSSOXML()

Func _EnterpriseAccess_CreateSSOXML()
	Local	$names, $accounts, _
			$ou = 'OU=EnterpriseAccess,OU=User Accounts,DC=seton,DC=org'

	ProgressOn('SSO Export', 'Extracting account information', 'Scanning Enterprise OU', Default, Default, 16)
	ProgressSet(0)

	ADGetObjectsInOUwithAttrib($Xport_List, $ou, '(objectclass=user)')

	If Not IsArray($Xport_List) Then
		ProgressOff()
		MsgBox(16,'Empty', 'There are currently no account in the Enterprise Access OU.'&@CRLF&'Exiting script')
		Exit
	EndIf
	ProgressSet(50, 'Account information retrieved')
	Sleep(2000)
	MsgBox(16,'Pause', 'Pause')
;~ 	$names = _Create_array_from_file()
;~ 	_ArrayDisplay($Xport_List) ; Display name and username
	$names = _Create_array_from_array($Xport_List)
;~ 	_ArrayDisplay($names)
	Dim $accounts = 0
	ProgressSet(75, 'Creating XML file')
	Sleep(2000)
	_Create_XML($names)
	ProgressSet(100, 'Export complete')
	Sleep(2000)
	ProgressOff()
	MsgBox(64,'Done','The XML file has been created.')
EndFunc   ;==>_EnterpriseAccess_CreateSSOXML

Func _Create_array_from_array($accounts)
	Dim $y = 0, $z = 0
	$rows = UBound($accounts, 1)
	;MsgBox(0,'',$rows)
	Dim $SSO_Import[$rows + 1][3]
	For $x = 0 To $rows - 1
		$SSO_Import[$z][0] = $accounts[$z][1]
		$lastname = _StringBetween($accounts[$z][0], '"', ',')
		$SSO_Import[$z][1] = $lastname[0]
		$firstname = _StringBetween($accounts[$z][0], ', ', '"')
		$SSO_Import[$z][2] = $firstname[0]
		$z = $z + 1
	Next
	_ArrayDelete($SSO_Import, UBound($SSO_Import, 1))
	Return $SSO_Import
EndFunc   ;==>_Create_array_from_array

Func _Create_XML($names)
	Local	$writeToXML, $rows, _
			$EnterpriseLoc = @ScriptDir, _
			$XML = $EnterpriseLoc & '\SSO_XML_'&@MDAY&'-'&@MON&'-'&@YEAR&'.xml'

	FileDelete($XML)
	$writeToXML = FileOpen($XML, 2)
	FileWriteLine($writeToXML, '<?xml version="1.0" encoding="ASCII"?><userList' & @CRLF)
	FileWriteLine($writeToXML, 'xmlns="http://www.sentillion.com"' & @CRLF)
	FileWriteLine($writeToXML, 'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"' & @CRLF)
	FileWriteLine($writeToXML, 'xsi:schemaLocation="http://www.sentillion.com uma.xsd">' & @CRLF)
	;start user loop
	Local $z = 0
	$rows = UBound($names, 1)
	For $x = 0 To $rows - 1
		Local $username = $names[$z][0], $lastname = $names[$z][1], $firstname = $names[$z][2]
		FileWriteLine($writeToXML, '<user><uid>' & $username & '</uid><sn>' & $lastname & '</sn><givenName> ' & $firstname & '</givenName>' & @CRLF)
		FileWriteLine($writeToXML, '<appEntryList>' & @CRLF)
		FileWriteLine($writeToXML, '<appEntry><logonSuffix>windows</logonSuffix><logonId>' & $username & '</logonId><password></password></appEntry>' & @CRLF)
		FileWriteLine($writeToXML, '<appEntry><logonSuffix>role</logonSuffix><logonId>clinician</logonId><password></password></appEntry>' & @CRLF)
		FileWriteLine($writeToXML, '</appEntryList></user>' & @CRLF)
		$z = $z + 1
	Next
	FileWriteLine($writeToXML, '</userList>' & @CRLF)
	FileClose($writeToXML)
EndFunc   ;==>_Create_XML

Func ADGetObjectsInOUwithAttrib(ByRef $aArray, $ou, $filter2 = '', $filter = "(name=*)", $searchscope = 2, $datatoretrieve = "sAMAccountName", $datatoretrieve2 = "Name", $sortby = "sAMAccountName")
	Dim $ObjectArray, $ObjectArray2
	Local $objRecordSet
	$objCommand = ObjCreate("ADODB.Command")
	$objCommand.ActiveConnection = $objConnection
	$objCommand.Properties("Page Size") = 256
	$objCommand.Properties("Searchscope") = $searchscope
	$objCommand.Properties("TimeOut") = 20

	If $filter2 = '' Then
		$strCmdText = "<LDAP://" & $strHostServer & "/" & $ou & ">;" & $filter & ";" & $datatoretrieve & ";subtree"
	Else
		$strCmdText = "<LDAP://" & $strHostServer & "/" & $ou & ">;" & '(&' & $filter2 & $filter & ')' & ";" & $datatoretrieve & ";subtree"
	EndIf

	If $filter2 = '' Then
		$strCmdText2 = "<LDAP://" & $strHostServer & "/" & $ou & ">;" & $filter & ";" & $datatoretrieve2 & ";subtree"
	Else
		$strCmdText2 = "<LDAP://" & $strHostServer & "/" & $ou & ">;" & '(&' & $filter2 & $filter & ')' & ";" & $datatoretrieve2 & ";subtree"
	EndIf

	$objCommand.CommandText = $strCmdText

	$objRecordSet = $objCommand.Execute

	$recordcount = $objRecordSet.RecordCount

	If $recordcount = 0 Then
		$objCommand = 0
		$objRecordSet = 0
		Return 0
	EndIf

	If StringInStr($datatoretrieve, ",") Then

		$dtrArray = StringSplit($datatoretrieve, ",")

		Dim $ObjectArray[$recordcount + 1][$dtrArray[0]]

		$ObjectArray[0][0] = $recordcount
		$ObjectArray[0][1] = $dtrArray[0]

		$count = 1
		$objRecordSet.MoveFirst
		Do
			For $i = 1 To $dtrArray[0]
				$ObjectArray[$count][$i - 1] = $objRecordSet.Fields($dtrArray[$i] ).Value
			Next

			$objRecordSet.MoveNext
			$count += 1
		Until $objRecordSet.EOF
	Else
		Dim $ObjectArray[$recordcount + 1]
		$ObjectArray[0] = UBound($ObjectArray) - 1
		If $ObjectArray[0] = 0 Then
			$ObjectArray = 0
			Return 0
		Else
			$count = 1
			$objRecordSet.MoveFirst
			Do
				$ObjectArray[$count] = $objRecordSet.Fields($datatoretrieve).Value
				$objRecordSet.MoveNext
				$count += 1
			Until $objRecordSet.EOF
		EndIf
	EndIf

	$objRecordSet = 0

	$objCommand.CommandText = $strCmdText2

	$objRecordSet = $objCommand.Execute

	$recordcount = $objRecordSet.RecordCount
	If $recordcount = 0 Then
		$objCommand = 0
		$objRecordSet = 0
		Return 0
	EndIf

	If StringInStr($datatoretrieve, ",") Then

		$dtrArray = StringSplit($datatoretrieve, ",")

		Dim $ObjectArray2[$recordcount + 1][$dtrArray[0]]

		$ObjectArray2[0][0] = $recordcount
		$ObjectArray2[0][1] = $dtrArray[0]

		$count = 1
		$objRecordSet.MoveFirst
		Do
			For $i = 1 To $dtrArray[0]
				$ObjectArray2[$count][$i - 1] = $objRecordSet.Fields($dtrArray[$i] ).Value
			Next

			$objRecordSet.MoveNext
			$count += 1
		Until $objRecordSet.EOF
	Else
		Dim $ObjectArray2[$recordcount + 1]
		$ObjectArray2[0] = UBound($ObjectArray) - 1
		If $ObjectArray2[0] = 0 Then
			$ObjectArray2 = 0
			Return 0
		Else
			$count = 1
			$objRecordSet.MoveFirst
			Do
				$ObjectArray2[$count] = $objRecordSet.Fields($datatoretrieve2).Value
				$objRecordSet.MoveNext
				$count += 1
			Until $objRecordSet.EOF
		EndIf
	EndIf

	$objCommand = 0
	$objRecordSet = 0
	_ArrayDelete($ObjectArray, 0)
	_ArrayDelete($ObjectArray2, 0)
	$rows = UBound($ObjectArray, 1)
	Dim $Xport_List[$rows][2]

	Local $y = 0
	For $x = 0 To $rows - 1
		$username = $ObjectArray[$y]
		$Full_Name = $ObjectArray2[$y]
		$Xport_List[$y][0] = '"' & $Full_Name & '"'
		$Xport_List[$y][1] = $username
		$y = $y + 1
	Next
EndFunc   ;==>ADGetObjectsInOUwithAttrib

